<?php

namespace App\Entity;

use App\Repository\MarqueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MarqueRepository::class)]
class Marque
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(targetEntity: Vehicule::class, mappedBy: 'marque')]
    private $vehiculs;

    #[ORM\Column(type: 'string')]
    private $nom;

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return ArrayCollection
     */
    public function getVehiculs(): ArrayCollection
    {
        return $this->vehiculs;
    }

    /**
     * @param ArrayCollection $vehiculs
     */
    public function setVehiculs(ArrayCollection $vehiculs): void
    {
        $this->vehiculs = $vehiculs;
    }


    public function __construct(){
        $this->vehiculs = new ArrayCollection();
    }

    public function addVehicul(Vehicule $vehicule){
        if(!$this->vehiculs->contains($vehicule)){
            $this->vehiculs->add($vehicule);
        }
    }

    public function removeVehicul(Vehicule $vehicule){
        if($this->vehiculs->contains($vehicule)){
            $this->vehiculs->removeElement($vehicule);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
