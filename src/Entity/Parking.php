<?php
namespace App\Entity;
use App\Repository\ParkingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ParkingRepository::class)]
class Parking{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(targetEntity: Place::class, mappedBy: 'parking')]
    private $places;

    #[ORM\Column(name: 'nom', type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(name: 'adresse', type: 'string', length: 255)]
    private $adresse;

    #[ORM\ManyToOne(targetEntity: Color::class, inversedBy: 'parkings')]
    private $color;

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }


    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }



    public function __construct(){
        $this->places = new ArrayCollection();
    }

    public function getPlaces(){
        return $this->places;
    }

    public function addPlace(Place $place){
        if(!$this->places->contains($place)){
            $this->places->add($place);
        }
    }


    public function getId()
    {
        return $this->id;
    }



}