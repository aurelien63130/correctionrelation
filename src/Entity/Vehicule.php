<?php

namespace App\Entity;

use App\Repository\VehiculeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VehiculeRepository::class)]
class Vehicule
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $immatriculation;

    #[ORM\ManyToOne(targetEntity: Marque::class, inversedBy: 'vehiculs')]
    private $marque;

    #[ORM\ManyToMany(targetEntity: Color::class, mappedBy: 'vehiculs')]
    private $colors;

    #[ORM\OneToOne(targetEntity: Place::class, mappedBy: 'vehicule')]
    private $place;

    #[ORM\Column(type: 'string')]
    private $model;

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model): void
    {
        $this->model = $model;
    }



    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place): void
    {
        $this->place = $place;
    }


    public function __construct(){
        $this->colors = new ArrayCollection();
    }

    public function addColor(Color $color){
        if(!$this->colors->contains($color)){
            $this->colors->add($color);
        }
    }

    public function removeColor(Color $color){
        if($this->colors->contains($color)){
            $this->colors->removeElement($color);
        }
    }

    public function getColors(){
        return $this->colors;
    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     */
    public function setMarque($marque): void
    {
        $this->marque = $marque;
    }


    /**
     * @return mixed
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * @param mixed $immatriculation
     */
    public function setImmatriculation($immatriculation): void
    {
        $this->immatriculation = $immatriculation;
    }



    public function getId(): ?int
    {
        return $this->id;
    }
}
