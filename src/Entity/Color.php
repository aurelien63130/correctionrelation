<?php

namespace App\Entity;

use App\Repository\ColorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ColorRepository::class)]
class Color
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToMany(targetEntity: Vehicule::class, inversedBy: 'colors')]
    private $vehiculs;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\OneToMany(targetEntity: Parking::class, mappedBy: 'color')]
    private $parkings;

    public function __construct(){
        $this->vehiculs = new ArrayCollection();
        $this->parkings = new ArrayCollection();
    }

    public function addParking(Parking $parking){
        if(!$this->parkings->contains($parking)){
            $this->parkings->add($parking);
        }
    }

    public function removeParking(Parking $parking){
        if($this->parkings->contains($parking)){
            $this->parkings->removeElement($parking);
        }
    }

    public function getParkings(){
        return $this->parkings;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }





    public function addVehicul(Vehicule $vehicule){
        if(!$this->vehiculs->contains($vehicule)){
            $this->vehiculs->add($vehicule);
        }
    }

    public function removeVehicule(Vehicule $vehicule){
        if($this->vehiculs->contains($vehicule)){
            $this->vehiculs->removeElement($vehicule);
        }
    }

    public function getVehicules(){
        return $this->vehiculs;
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return ArrayCollection
     */
    public function getVehiculs(): ArrayCollection
    {
        return $this->vehiculs;
    }

    /**
     * @param ArrayCollection $vehiculs
     */
    public function setVehiculs(ArrayCollection $vehiculs): void
    {
        $this->vehiculs = $vehiculs;
    }

}
