<?php
namespace App\Entity;
use App\Repository\PlaceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PlaceRepository::class)]
class Place{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;


    #[ORM\ManyToOne(targetEntity: Parking::class, inversedBy: 'places')]
    private $parking;

    #[ORM\Column(type: 'integer', name: 'numero_place')]
    private $numPlace;

    #[ORM\OneToOne(targetEntity: Vehicule::class, inversedBy: 'place')]
    #[ORM\JoinColumn(nullable: true)]
    private $vehicule;


    public function getVehicule()
    {
        return $this->vehicule;
    }


    public function setVehicule($vehicule): void
    {
        $this->vehicule = $vehicule;
    }

    public function getNumPlace()
    {
        return $this->numPlace;
    }

    /**
     * @param mixed $numPlace
     */
    public function setNumPlace($numPlace): void
    {
        $this->numPlace = $numPlace;
    }



    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getParking()
    {
        return $this->parking;
    }
}
